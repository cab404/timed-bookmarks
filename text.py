MESSAGE_INTRO = """
I usually skip through a bunch of things that are "nice to know, but not now", or fill my time in public transport with random stuff from HN.

Ideally, this bot solves that. 

1. You send a thing to it.
2. You specify reading time for it via buttons.
3. When you need to fill your time, you press a button, and a thing from reading list pops up.

__Implementation notice: I don't actually save anything you send, only message IDs.__
"""[1:]

MESSAGE_GOT_IT = """
Got it. Default duration: {duration}.
"""[1:]

BUTTONS = {
    "Get 10 min. task": 10,
    "Get 30 min. task": 30,
    "Get 90 min. task": 90
}
INLINE_BUTTONS = {
    "10 min": 10,
    "30 min": 30,
    "90 min": 90
}

USER_DELETED = "Bookmark data cleared. Goodbye?"

BOOKMARK_ADDED = "Added. Default duration: 10 minutes."

NO_BOOKMARKS = "No saved bookmarks of that duration :/"

ESTIMATE_CHANGED = "Estimate changed."