import random
from builtins import FileNotFoundError
from typing import Dict

from telegram import Update, CallbackQuery, ReplyKeyboardMarkup, KeyboardButton, InlineKeyboardButton, \
    InlineKeyboardMarkup, Message
from telegram.ext import Updater, CallbackContext, CommandHandler, MessageHandler, Filters, CallbackQueryHandler
import json
import logging

from text import MESSAGE_INTRO, BUTTONS, BOOKMARK_ADDED, USER_DELETED, NO_BOOKMARKS, ESTIMATE_CHANGED, INLINE_BUTTONS

logging.basicConfig(
    level=logging.DEBUG
)

with open("secret.json") as file:
    config = json.load(file)

bookmarks: Dict[int, Dict[int, int]] = {}


def delete_bookmarks(user: int):
    if user in bookmarks:
        del bookmarks[user]


def save_bookmark(user: int, bookmark: int, time: int):
    bookmarks.setdefault(user, {})
    bookmarks[user][bookmark] = time


def update_estimate_bookmark(user: int, bookmark: int, time: int):
    save_bookmark(user, bookmark, time)


def find_bookmark(user: int, time: int) -> int:
    bookmarks.setdefault(user, {})

    user_bookmarks = bookmarks[user]
    print(user_bookmarks)
    filtered = \
        list(filter(
            lambda t: t[1] == time,
            user_bookmarks.items()
        ))

    if len(filtered) == 0:
        raise FileNotFoundError
    else:
        msg_id, duration = random.choice(filtered)
        del user_bookmarks[msg_id]
        return msg_id


######################################################

tg = Updater(
    token=config["token"],
    use_context=True,
    request_kwargs=config["requests_kwargs"]
)


def start(update: Update, context: CallbackContext):
    context.bot.send_message(
        update.effective_chat.id, MESSAGE_INTRO,
        parse_mode="Markdown", reply_markup=ReplyKeyboardMarkup(
            keyboard=[list(KeyboardButton(a) for a in BUTTONS.keys())]
        )
    )


def handle_message(update: Update, context: CallbackContext):
    msg: Message = update.message

    if msg.text is not None:
        if msg.text in BUTTONS.keys():
            get_bookmark(update, context, BUTTONS[msg.text])
            return

    save_bookmark(update.effective_chat.id, msg.message_id, 10)
    context.bot.send_message(
        update.effective_chat.id, BOOKMARK_ADDED,
        reply_markup=InlineKeyboardMarkup(
            inline_keyboard=[
                [
                    list(
                        InlineKeyboardButton(text, callback_data=f"{msg.message_id}#{minutes}")
                        for text, minutes
                        in INLINE_BUTTONS.keys()
                    )
                ]
            ]
        ))


def callback_query_handler(update: Update, context: CallbackContext):
    cq: CallbackQuery = update.callback_query
    msg, est = cq.data.split("#")
    update_estimate_bookmark(update.effective_chat.id, int(msg), int(est))
    context.bot.answer_callback_query(cq.id, text=ESTIMATE_CHANGED)


def get_bookmark(update: Update, context: CallbackContext, duration: int):
    try:
        msg_id = find_bookmark(update.effective_chat.id, duration)
        context.bot.forward_message(update.effective_chat.id, update.effective_chat.id, msg_id)
    except FileNotFoundError:
        context.bot.send_message(update.effective_chat.id, NO_BOOKMARKS)


def delete_user_data(update: Update, context: CallbackContext):
    delete_bookmarks(update.effective_chat.id)
    context.bot.send_message(update.effective_chat.id, USER_DELETED)


tg.dispatcher.add_handler(CommandHandler("start", start))
tg.dispatcher.add_handler(CommandHandler("unplug_me", delete_user_data))
tg.dispatcher.add_handler(MessageHandler(Filters.all, handle_message))
tg.dispatcher.add_handler(CallbackQueryHandler(callback_query_handler))

tg.start_polling()
