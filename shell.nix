let
    basepkgs = import <nixpkgs> {};

    pkgs = import (basepkgs.fetchFromGitHub {
        owner = "nixos";
        repo = "nixpkgs";
        rev = "0de211d08d13ce3ad1ed055c2cdfb1b18e06f3d6";
        sha256 = "0316p045dpbq83dsg0k8c6akpcllz21xm1aanxg1fy7m6bk38s8b";
    }) {};

    src = pkgs.lib.cleanSource ./.;
    pyproject = ./pyproject.toml;
    poetrylock = ./poetry.lock;
    python = pkgs.python3;

in
    pkgs.poetry2nix.mkPoetryApplication {
        inherit poetrylock python src pyproject;
    }
